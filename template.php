<?php

/**
 * @file
 * template.php
 */

/**
 * Implements hook_preprocess_page().
 */
function pm_kickstart_theme_preprocess_page(&$variables) {
  $variables['sidebar_first_status'] = 'sidebar-first-present';
  if (empty($variables['page']['sidebar_first']) AND empty($variables['main_menu'])) {
    $variables['sidebar_first_status'] = 'sidebar-first-absent';
  }
  if (theme_get_setting('alter_vertical_tabs')) {
    drupal_add_js(drupal_get_path('theme', 'pm_kickstart_theme') .'/js/_vertical-tabs.js', 'file');
  }
  $variables['wrapper_toggle_class'] = '';
  if (isset($_COOKIE['Drupal_pmKickstartTheme_sidebar_toggle'])) {
    $variables['wrapper_toggle_class'] = $_COOKIE['Drupal_pmKickstartTheme_sidebar_toggle'];
  }
  // Select minimalist login screen template for anonymous users.
  if (!empty($variables['user']) AND empty($variables['user']->uid) AND arg(0) == 'user') {
    $variables['theme_hook_suggestions'][] = 'page__pmkickstart_minimalist_user_login';
  }
}

/**
 * Implements hook_preprocess_views_view_table().
 */
function pm_kickstart_theme_preprocess_views_view_table(&$vars) {
  $vars['classes_array'][] = 'table table-stripped table-bordered table-hover';
}


function pm_kickstart_theme_preprocess_ctools_dropdown(&$vars) {
  $vars['dropdown_menu'] = array();
  $vars['default_link']  = array();

  $flag_first_item = TRUE;
  foreach ($vars['links'] as $key => $value) {
    $options = array();
    $href = $value['href'];
    if (isset($value['query'])) {
      $options = array(
        'query' => $value['query'],
      );
    }
    $url = !empty($href) ? check_plain(url($href, $options)) : '';

    if ($flag_first_item) {
      $vars['default_link'] = $value;
      $vars['default_link']['url'] = $url;
      $vars['default_link']['class'] = !empty($value['attributes']['class']) ? implode(' ', $value['attributes']['class']) : '';
    }
    else {
      $vars['dropdown_menu'][$key] = $value;
      $vars['dropdown_menu'][$key]['url'] = $url;
      $vars['dropdown_menu'][$key]['class'] = !empty($value['attributes']['class']) ? implode(' ', $value['attributes']['class']) : '';
    }
    $flag_first_item = FALSE;
  }
}

function pm_kickstart_theme_preprocess_links__ctools_dropbutton(&$vars) {
  $vars['dropdown_menu'] = array();
  $vars['default_link']  = array();

  $flag_first_item = TRUE;
  foreach ($vars['links'] as $key => $value) {
    if (isset($value['attributes']['class'])) {
      if ($key = array_search('icon compact add', $value['attributes']['class']) !== FALSE) {
        unset($value['attributes']['class'][0]);
        $value['title'] = '<i class="fa fa-plus"></i> ' . $value['title'];
        $value['attributes']['class'][] = 'btn';
        $value['attributes']['class'][] = 'btn-default';
      }
      if ($key = array_search('icon compact rearrange', $value['attributes']['class']) !== FALSE) {
        unset($value['attributes']['class'][0]);
        $value['title'] = '<i class="fa fa-gear"></i> ' . $value['title'];
        $value['attributes']['class'][] = 'btn';
        $value['attributes']['class'][] = 'btn-default';
      }
    }

    $options = array();
    $href = $value['href'];
    if (isset($value['query'])) {
      $options = array(
        'query' => $value['query'],
      );
    }
    $url = !empty($href) ? check_plain(url($href, $options)) : '';

    if ($flag_first_item) {
      $vars['default_link'] = $value;
      $vars['default_link']['url'] = $url;
      $vars['default_link']['class'] = !empty($value['attributes']['class']) ? implode(' ', $value['attributes']['class']) : '';
      $vars['default_link'][$key]['html'] = (isset($value['html'])) ? $value['html'] : FALSE;
    }
    else {
      $vars['dropdown_menu'][$key] = $value;
      $vars['dropdown_menu'][$key]['url'] = $url;
      $vars['dropdown_menu'][$key]['class'] = !empty($value['attributes']['class']) ? implode(' ', $value['attributes']['class']) : '';
      $vars['dropdown_menu'][$key]['html'] = (isset($value['html'])) ? $value['html'] : FALSE;
    }
    $flag_first_item = FALSE;
  }
}

/**
 * Theme function implementation for bootstrap_search_form_wrapper.
 */
function pm_kickstart_theme_bootstrap_search_form_wrapper($variables) {
  $parent = reset($variables['element']['#parents']);
  $prefix = TRUE;
  $button_type = 'button';
  $btn_classes = 'btn btn-default disabled mock-button';
  switch ($parent) {
    case 'name':
      $fa_icon = 'glyphicon-user';
      break;
    case 'pass':
      $fa_icon = 'glyphicon-asterisk';
      break;
    case 'search_block_form':
      $fa_icon = 'glyphicon-search';
      $button_type = 'submit';
      $btn_classes = 'btn btn-default';
      $prefix = FALSE;
      break;
    default:
      $fa_icon = 'glyphicon-pencil';
      break;
  }

  $button  = '<span class="input-group-btn">';
  $button .= '<button type="' . $button_type . '" tabIndex="-1" class="' . $btn_classes . '">';
  $button .= '<i class="glyphicon  '. $fa_icon .' fa-fw"></i>';
  $button .= '</button>';
  $button .= '</span>';

  $output  = '<div class="input-group">';

  if ($prefix) {
    $output .= $button . $variables['element']['#children'];
  }
  else {
    $output .= $variables['element']['#children'] . $button;
  }

  $output .= '</div>';

  return $output;
}


/**
 * Implements hook_form_alter().
 */
function pm_kickstart_theme_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
  if ($form_id) {
    switch ($form_id) {
      case 'user_login_form':
      case 'user_login_block':
        $form['name']['#theme_wrappers'] = array('bootstrap_search_form_wrapper');
        $form['pass']['#theme_wrappers'] = array('bootstrap_search_form_wrapper');
        break;

      case 'search_block_formdddd':
        $form['#attributes']['class'][] = 'form-search';

        $form['search_block_form']['#title'] = '';
        $form['search_block_form']['#attributes']['placeholder'] = t('Search');

        // Hide the default button from display and implement a theme wrapper
        // to add a submit button containing a search icon directly after the
        // input element.
        $form['actions']['submit']['#attributes']['class'][] = 'element-invisible';
        $form['search_block_form']['#theme_wrappers'] = array('bootstrap_search_form_wrapper');

        // Apply a clearfix so the results don't overflow onto the form.
        $form['#attributes']['class'][] = 'content-search';
        break;
    }

  }
}

/**
 * Implements hook_theme
 */
function pm_kickstart_theme_theme() {
  return array(
    'user_login' => array(
      'render element' => 'form',
      'template' => 'user-login',
      'arguments' => array('form' => NULL),
      'path' => drupal_get_path('theme', 'pm_kickstart_theme') . '/templates',
    ),
    'user_register_form' => array(
      'render element' => 'form',
      'template' => 'user-register',
      'arguments' => array('form' => NULL),
      'path' => drupal_get_path('theme', 'pm_kickstart_theme') . '/templates',
    ),
    'user_pass' => array(
      'render element' => 'form',
      'template' => 'user-pass',
      'arguments' => array('form' => NULL),
      'path' => drupal_get_path('theme', 'pm_kickstart_theme') . '/templates',
    ),
  );
}

function pm_kickstart_theme_preprocess_user_login(&$variables) {
  $variables['intro_text'] = t('Login');
  pm_kickstart_theme_user_form_links($variables);
}

function pm_kickstart_theme_preprocess_user_register_form(&$variables) {
  $variables['intro_text'] = t('Register');
  pm_kickstart_theme_user_form_links($variables);
}

function pm_kickstart_theme_preprocess_user_pass(&$variables) {
  $variables['intro_text'] = t('Forgot Password');
  pm_kickstart_theme_user_form_links($variables);
}

function pm_kickstart_theme_user_form_links(&$variables) {
  $variables['rendered'] = drupal_render_children($variables['form']);

  $variables['forgot_password_link'] = (drupal_valid_path('user/password')) ? l(t('Forgot password?'), 'user/password') : '';
  $variables['new_user_register_link'] = (drupal_valid_path('user/register')) ? l(t('Register?'), 'user/register') : '';
  $variables['user_login_link'] = (drupal_valid_path('user/login')) ? l(t('Login?'), 'user/login') : '';
}

function pm_kickstart_theme_preprocess_links(&$vars) {
  if (isset($vars['attributes']['class']) AND ($key = array_search('links inline', $vars['attributes']['class']) !== FALSE)) {
    $vars['attributes']['class'][$key] = 'list-inline';
  }
}

function pm_kickstart_theme_preprocess_ctools_dropbutton(&$vars) {
  $vars['dropdown_menu'] = array();
  $vars['default_link']  = array();

  $flag_first_item = TRUE;
  foreach ($vars['links'] as $key => $value) {
    $options = array();
    $href = $value['href'];
    if (isset($value['query'])) {
      $options = array(
        'query' => $value['query'],
      );
    }
    $url = !empty($href) ? check_plain(url($href, $options)) : '';

    if ($flag_first_item) {
      $vars['default_link'] = $value;
      $vars['default_link']['url'] = $url;
      $vars['default_link']['class'] = !empty($value['attributes']['class']) ? implode(' ', $value['attributes']['class']) : '';
    }
    else {
      $vars['dropdown_menu'][$key] = $value;
      $vars['dropdown_menu'][$key]['url'] = $url;
      $vars['dropdown_menu'][$key]['class'] = !empty($value['attributes']['class']) ? implode(' ', $value['attributes']['class']) : '';
    }
    $flag_first_item = FALSE;
  }
}

function pm_kickstart_theme_ctools_collapsible($vars) {
  $class = $vars['collapsed'] ? ' ctools-collapsed' : '';
  $output = '<div class="xoxo ctools-collapsible-container' . $class . '">';
  $output .= '<div class="ctools-collapsible-handle">' . $vars['handle'] . '</div>';
  $output .= '<div class="ctools-collapsible-content">' . $vars['content'] . '</div>';
  $output .= '</div>';
  return $output;
}

function pm_kickstart_theme_preprocess_pm_dashboard(&$vars) {
  if (isset($vars['links']) && is_array($vars['links'])) {
    foreach ($vars['links'] as &$rows) {
      foreach ($rows as &$link) {
        $link = array(
          '#theme' => isset($link['theme']) ? $link['theme'] : 'pm_dashboard_link',
          '#link_blocks' => $link,
        );
      }
    }
  }
}

function pm_kickstart_theme_preprocess_pm_dashboard_link(&$vars) {
  module_load_include('inc', 'pm', 'includes/pm.icon');
  $link = $vars['link_blocks'];
  $vars['fa_icon'] = pm_helper_get_fa_icon($link['icon']);
  $vars['href'] = check_plain(url($link['path']));
  $vars['href_add'] = FALSE;
  if (isset($link['add_type']) AND $link['add_type']) {
    $vars['href_add'] = url('node/add/' . check_plain($link['add_type']));
  }
  elseif (isset($link['extra_link']) AND $link['extra_link']) {
    $vars['href_add'] = url($link['extra_link']);
  }
}


/**
 * Implements hook_preprocess_breadcrumb().
 */
function pm_kickstart_theme_preprocess_breadcrumb(&$variables) {
  $breadcrumb = &$variables['breadcrumb'];
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'class' => array(
        'breadcrumb-home'
      ),
    ),
  );
  $home_link = l('<i class="glyphicon glyphicon-home"></i>','<front>', $options);
  $breadcrumb = array($home_link) + $breadcrumb;

  $item = menu_get_item();
  $breadcrumb[] = array(
    // If we are on a non-default tab, use the tab's title.
    'data' => !empty($item['tab_parent']) ? check_plain($item['title']) : drupal_get_title(),
    'class' => array('active'),
  );
}
