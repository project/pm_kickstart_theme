<div id="pmdashboard" class="row">
  <div class="dashboard col-lg-6 col-md-6 col-sm-12">
    <?php if (isset($links[0]) AND is_array($links[0])): ?>
      <?php foreach ($links[0] as $link): ?>
        <?php echo drupal_render($link); ?>
      <?php endforeach ?>
    <?php endif ?>
  </div>
  <div class="dashboard col-lg-6 col-md-6 col-sm-12">
    <?php if (isset($links[1]) AND is_array($links[1])): ?>
      <?php foreach ($links[1] as $link): ?>
        <?php echo drupal_render($link); ?>
      <?php endforeach ?>
    <?php endif ?>
  </div>
</div>
