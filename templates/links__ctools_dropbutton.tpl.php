<!-- Split button -->
<div class="btn-group">
  <?php if (isset($default_link['html']) AND $default_link['html']): ?>
    <?php echo $default_link['title']; ?>
  <?php elseif (!empty($default_link['url'])): ?>
    <a href="<?php echo $default_link['url']; ?>" class="btn btn-default <?php echo $default_link['class'] ?>"><?php echo $default_link['title']; ?></a>
  <?php else: ?>
    <?php echo $default_link['title']; ?>
  <?php endif ?>
  <?php if (!empty($dropdown_menu)): ?>
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      <span class="caret"></span>
      <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <?php foreach ($dropdown_menu as $link): ?>
        <li>
          <?php if ($link['html']): ?>
            <?php echo $link['title']; ?>
          <?php elseif (!empty($link['url'])): ?>
            <a href="<?php echo $link['url']; ?>" class="<?php echo $link['class']; ?>"><?php echo $link['title']; ?></a>
          <?php else: ?>
            <?php echo $link['title']; ?>
          <?php endif ?>
        </li>
      <?php endforeach ?>
    </ul>
  <?php endif ?>
</div>
