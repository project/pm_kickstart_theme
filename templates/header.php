<header id="navbar" role="banner" class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="pull-left sidebar-toggle-button navbar-toggle">
      <span class="sr-only">Toggle Sidebar</span>
      <i class="glyphicon glyphicon-chevron-right"></i>
    </button>
    <div class="branding">
      <?php if ($logo): ?>
      <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
      <?php endif; ?>

      <?php if (!empty($site_name)): ?>
      <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>
    </div>
    <button class="pull-right navbar-toggle" type="button" data-toggle="collapse" data-target="#secondary-nav">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
  <?php if (!empty($page['header'])): ?>
    <div class="pull-left header">
      <?php print render($page['header']); ?>
    </div>
  <?php endif; ?>
  <div id="secondary-nav" class="pull-right secondary-nav collapse navbar-collapse col-xs-12">
    <?php if (!empty($secondary_nav)): ?>
        <nav role="navigation">
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
        </nav>
    <?php endif; ?>
    <?php if (!empty($page['navigation'])): ?>
      <?php print render($page['navigation']); ?>
    <?php endif; ?>
  </div>
</header>