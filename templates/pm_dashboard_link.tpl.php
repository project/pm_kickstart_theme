<div class="pm-dashboard-link well-sm">
  <div class="btn-group" role="group" aria-label="...">
    <a href="<?php echo $href ?>" class="btn btn-default btn-lg pm-dashboard-link-main">
      <i class="fa pm-icon fa-fw <?php echo $fa_icon; ?>"></i>
      <span>
        <?php echo $title ?>
      </span>
    </a>
    <?php if ($href_add ): ?>
      <a href="<?php echo $href_add ?>" class="btn btn-info btn-lg pm-dashboard-link-add">&nbsp;<span class="glyphicon glyphicon-plus"></span>&nbsp;</a>
    <?php endif ?>
  </div>
</div>
