<!-- Sidebar -->
<div id="sidebar-wrapper" class="sidebar-wrapper">
  <button type="button" class="pull-right sidebar-toggle-button slide-close navbar-toggle" style="z-index: 1;">
    <span class="sidebar-shrink-button glyphicon glyphicon-remove-circle"></span>
  </button>
  <button id="sidebar-shrink-button" class="btn hidden-xs">
    <span class="glyphicon glyphicon-menu-left pull-left"></span>
  </button>
    <div class="navbar-default sidebar clearfix" role="navigation">
      <div class="sidebar-nav clearfix">
        <?php if (!empty($primary_nav)): ?>
            <nav role="navigation">
              <?php if (!empty($primary_nav)): ?>
                <?php print render($primary_nav); ?>
              <?php endif; ?>
            </nav>
        <?php endif; ?>
      </div>
      <?php if (!empty($page['sidebar_first'])): ?>
        <div class="sidebar-first-wrapper">
          <?php print render($page['sidebar_first']); ?>
        </div>
      <?php endif; ?>
    </div>
</div>
<!-- /#sidebar-wrapper -->