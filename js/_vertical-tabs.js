
(function ($) {

/**
 * This script transforms a set of fieldsets into a stack of vertical
 * tabs. Another tab pane can be selected by clicking on the respective
 * tab.
 *
 * Each tab may have a summary which can be updated by another
 * script. For that to work, each fieldset has an associated
 * 'verticalTabCallback' (with jQuery.data() attached to the fieldset),
 * which is called every time the user performs an update to a form
 * element inside the tab pane.
 */
Drupal.behaviors.verticalTabs = {
  attach: function (context) {
    $('.vertical-tabs-panes', context).once('vertical-tabs', function () {
      // Check if there are some fieldsets that can be converted to vertical-tabs
      var $fieldsets = $('> fieldset', this);
      if ($fieldsets.length == 0) {
        return;
      }
      $(this).wrap('<div class="pm-kickstart-theme-vertical-tabs-accordion clearfix"></div>');
      // Transform each fieldset into a tab.
      $fieldsets.each(function () {
        var vertical_tab = new Drupal.verticalTab({
          title: $('> .panel-heading', this).html(),
          fieldset: $(this),
          body: $('> .panel-body', this)
        });
        vertical_tab.body.wrap('<div class="panel-body-wrapper collapse"></div>');
        $(this).find('> .panel-heading').html(vertical_tab.item);
        $(this).data('verticalTab', vertical_tab)
      });
    });
  }
};

/**
 * The vertical tab object represents a single tab within a tab group.
 *
 * @param settings
 *   An object with the following keys:
 *   - title: The name of the tab.
 *   - fieldset: The jQuery object of the fieldset that is the tab pane.
 */
Drupal.verticalTab = function (settings) {
  var self = this;
  $.extend(this, settings, Drupal.theme('verticalTab', settings));

  this.fieldset
    .bind('summaryUpdated', function () {
      self.updateSummary();
    })
    .trigger('summaryUpdated');
};

Drupal.verticalTab.prototype = {
  /**
   * Updates the tab's summary.
   */
  updateSummary: function () {
    this.summary.html(this.fieldset.drupalGetSummary());
  }
};

/**
 * Theme function for a vertical tab.
 *
 * @param settings
 *   An object with the following keys:
 *   - title: The name of the tab.
 * @return
 *   This function has to return an object with at least these keys:
 *   - item: The root tab jQuery element
 *   - link: The anchor tag that acts as the clickable area of the tab
 *       (jQuery version)
 *   - summary: The jQuery element that contains the tab summary
 */
Drupal.theme.prototype.verticalTab = function (settings) {
  var tab = {};
  tab.summary = $('<small class="summary" role="tab" id="#summaryFor' + settings.fieldset[0].id + '"></small>');
  tab.title = $('<h4 class="panel-title"></h4>').text(settings.title);
  tab.title = $(settings.title);
  tab.link = $('<button href="#" class="pmkickstart-accordion-toggle" type="button" data-toggle="collapse" data-target="#' +  settings.fieldset[0].id +  ' > .panel-body-wrapper" aria-expanded="false" aria-controls="collapseExample"></button>')
  tab.item = $('<div class="panel-heading" tabindex="-1"></div>');
  tab.link.append(tab.title);
  tab.link.append(tab.summary);
  tab.item.append(tab.link);
  return tab;
};

})(jQuery);
